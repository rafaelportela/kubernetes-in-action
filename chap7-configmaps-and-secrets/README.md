# Chap 7 ConfigMaps and Secrets: configuring application


## 7.2 Passing command-line arguments to containers

*Understanding ENTRYPOINT and CMD*

* ENTRYPOINT defines the executable invoked when the container is started.
* CMD specify arguments that get passed to the ENTRYPOINT.

```
# Run ENTRYPOINT with CMD args
docker run <image>

# Override CMD from Dockerfile
docker run <image> <arguments>
```

*Understanding the difference between SHELL and EXEC*

* shell: `ENTRYPOINT node app.js` executes the application within a shell.
* exec: `ENTRYPOINT ["node", "app.js"]` executes the app directly as PID 1.

The shell process is usually unnecessary, which is why you should always use
the exec form of ENTRYPOINT.

```
#
# Use default CMD args
#
$ docker run -it docker.io/luksa/fortune:args
Configured to generate new fortune every 10 seconds

#
# Override CMD with command-line args
#
$ docker run -it docker.io/luksa/fortune:args 15
Configured to generate new fortune every 15 seconds
```

### 7.2.2 Overriding the command and arguments in Kubernetes

* ENTRYPOINT -> command
* CMD -> args

The command and args fields can't be updated after the pod is created.

```
apiVersion: v1
kind: Pod
metadata:
  name: fortune2s
spec:
  containerss:
    - image: luksa/fotune:args
      args: ["2"]
      name: html-generator
      volumeMounts:
        - name: html
          mountPath: /var/htdocs
   ...
```

You can also use the array notation (numbers should be string-quoted):
```
args:
  - foo
  - bar
  - "15"
```

## 7.3 Setting environment variables for a container

Env vars can be specified for each containers, but there's no option for
specify them at the pod level. Just like command and args, env vars can't be
updated after the pod is created.

### 7.3.1 Specifying environment variables in a container definition

```
kind: Pod
spec:
  containers:
  - image: luksa/fortune:env
    env:
      - name: INTERVAL    # single env var
        value: "30"
    name: html-generator
  ...
```

Kubernetes also inject env vars for each service in the same namespace.

### 7.3.2 Referring to other environment variables in a variable's value

You can reference previously defined env vars or other existing vars by using
the `$(VAR)` syntax.

```
env:
  - name: FIRST_VAR
    value: "foo"
  - name: SECOND_VAR
    value: "$(FIRST_VAR)bar"
```

Having values hardcoded in the pod definition means you need to have separate
pod definitions for production and your dev pods. To reuse the same pod
definition in multiple envs, it makes sense to decouple the configuration from
the pod descriptor.


## 7.4 Decouple configuration with a ConfigMap

An application doesn't need to read the ConfigMap derectly or even know that it
exists. The contents of the mpa are instead passed to containers as either env
vars or as files in a volume. You can also pass ConfigMap entries to process as
command-line args.

You can keep multiple ConfigMap manisfests with the same name, each for
a different environment.

### 7.4.2 Creating a ConfigMap

*Using the kubectl create configmap command*

```
kubectl create configmap fortune-config \
    --from-literal=sleep-internal=25
    --from-literal=foo=bar
    --from-literal=one=two

kubectl get configmap fortune-config -o yaml
kubectl create -f fortune-config.yaml
```

*Creating a ConfigMap entry from the contents of a file*

Reading files from disk and storing them as individual entries is supported in
ConfigMaps.

```
kubectl create configmap my-config --from-file=customkey=config-file.conf
```

You can combine from-literal and from-file a single create command.

### 7.4.3 Passing a ConfigMap entry to a container as an environment variable

```
apiVersion: v1
kind: Pod
metadata:
  name: fortune-env-from-configmap
spec:
  containers:
    - image: luksa/fortune:env
      env:
      - name: INTERVAL
        valueFrom:
          configMapKeyRef:
            name: fortune-config
            key: sleep-interval
  ...
```

A container referencing a non-existing ConfigMap will fail to start. If you
then create the missing ConfigMap, the failed container is started without
requiring you to recreate the pod.


You can also mark a refence to a COnfigMap as optional by setting
`configMapKeyRef.optional: true`. In this case, the container starts even if
the ConfigMap doesn't exist.

### 7.4.4 Passing all entries of a ConfigMap as environment variables at once

```
spec:
  containers:
    - image: some-image
      envFrom:
        - prefix: CONFIG_
          configMapRef:
            name: my-config-map
```

Env var key names can't contain dashes. So a key `FOO-BAR=value` in a ConfigMap
would be ignored and would not get exposed to the container.

### 7.4.5 Passing a ConfigMap entry as a command-line argument

```
apiVersion: v1
kind: Pod
metadata:
  name: fortune-args-from-configmap
spec:
  containers:
  - image: luksa/fortune:args
    env:
     - name: INTERVAL
       valueFrom:
         configMapKeyRef:
           name: fortune-config
           key: sleep-interval
    args: ["$(INTERVAL)"]
  ...
```

### Using a configMap volume to expose ConfigMap entries as files

Although this method is mostly meant for passing large config files to the
container, nothing prevents you from passing short single values this way.

```
$ tree
configmap-files/            # dir
    my-nginx-config.conf    #   file
    sleep-interval          #   file
```

```
$ kubectl creeate configmap fortune-config --from-file=configmap-files
$ kubectl get configmap fortune-config -o yaml
apiVersion: v1
data:
  my-nginx-config.conf: |
    server {
      listen        80;
      server_name   www.kubia.com;
      ...
    }
  sleep-interval: |
    25
kind: ConfigMap
```

Pipeline character -> literal multi-line value

*Using the ConfigMap's entries in a volume*

```
apiVersion: v1
kind: Pod
metadata:
  name: fortune-configmap-volume
spec:
  containers:
    - image: nginx:alpine
      name: web-server
      volumeMounts:
       - name: config
         mountPath: /etc/nginx/conf.d   # configmap volume mount destination
                                        # nginx reads all config files in this dir
         readOnly: true
  volumes:
    - name: config
      configMap:
      name: fortune-config
      defaultMode: "6600"               # optional: all files -rw-rw---- --
```

You could create different configmaps to configure containers in the same pod,
but usually it's fine to keep it all in a single configmap for the entire pod.
After all, having containers in the same pod implies that the containers are
closely related and should probably also be configured as a unit.

*Exposing certain ConfigMap entries in the volume*

```
volumes:
  - name: config
    configMap:
      name: fortune-config
      items:                         # select entries by listing them
        - key: my-nginx-config.conf  #   include this key
          path: gzip.conf            #   store value in this file
```

If you mount a volume as a directory, the previous contents of the dir will be
shadowed, not accessible. It's not a problem if the dir is originally empty,
but it may cause problems if the new mount hides important files, like a new
mount in `/etc`.

You may want to mount a volume using subPath:
```
spec:
  containers:
    - image: some/image
      volumeMounts:
       - name: myvolume
         mountPath: /etc/something.conf   # mount into a file, not a dir
         subPath: myconfig.conf           # instead of the whole volume,
                                          #   mount only the myconfig.conf entry
```

*Updating an app's config without having to restart the app*

Whey you update a ConfigMap, the files in all the volumes referencing it are
update. It's then up to the process to detect that they've been changed and
reload them.

```
# open editor to update
kubectl edit configmap fortune-configmap-config

kubectl exec fortune-configmap-volume            # pod name
    -c web-server                                # container name
    cat /etc/nginx/conf.d/my-nginx-config.conf   # command

kubectl exec fortune-configmap-volume
    -c web-server
    -- nginx -s reload                           # -- says -s param belongs to nginx command,
                                                 # not to be captured by kubectl
```

Kubernetes uses sym links to update all files atomically. It creates a new dir,
updates all files in it, then updates the existing path as a sym link to point
to the new directory.

One big caveat relates to updating ConfigMap-backed volumes. If you've mounted
a single file in the container instead of the whole volume, the file will not
be updated. One workaround is to mount the whole colume into a different
directory and then create a symbolic link pointing to the file in question.

Files in the ConfigMap volumes aren't updated synchronously across all running
instances, so files in individual pods may be out of sync for up to a whole
minute.


## 7.5 Using Secrets to pass sensitive data to containers

Like ConfigMaps, they're alse maps that hold key-value pair. Can be used the
same way as a ConfigMap:

* pass secret entries to container as env vars
* expose secret entries as files in a volume

Kubernetes only distribute the Secret objects to kubernetes' nodes that that
run pods that need access to a given secret. Also, Secrets are always kept in
memory, are never writting to disk. In ectd store, they are also encrypted.

### 7.5.2 Introducing the default token Secret

Every pod has a `secret` volume attached to it automatically - it appears in
the describe output of a pod. By default, it contains the defaut-token secret
with 3 items.

```
kubectl describe secrets
kubectl exec mypod ls /var/run/secrets/kubernetes.io/serviceaccount/
```

### 7.5.3 Creating a Secret

```
openssl genrsa -out https.key 2048
openssl req -new -x509 -key https.key -out https.cert -days 3650 \
    /CN=www.kubia-example.com

# additional foo file
echo bar > foo

kubectl create secret generic fortune-https \
    --from-file=https.key \
    --from-file=https.cert \
    --from-file=fortune-https   # opt: the whole dir could've been used, \
                                # instead of each file separatelly \
    --from-files=foo
```

This is creating a `generic` secret. A `tls` Secret could be created instead.
This would create the Secret with different entry names.

Contents of secrets are shown as Base64. Max size of a secret is 1MB.

### 7.5.5 Using the Secret in a pod

When you expose the Secret to a container through a secret volume, the value of
the secret entry is decoded and written to the file in its actual form
(regardless if it's plain text or binary). The same is true when exposing the
Secret entry through an environment variable.

```
spec:
  containers:
    - image: nginx:alpine
      name:  web-server
      volumeMounts:
        - name: certs
          moutnPath: /etc/nginx/certs/
          readOnly: true

volumes:
  - name: certs
    secret:
      secretName: fortune-https
```

Like ConfigMap volumes, secret volumes also support specifying file permissions
for the files exposed in the volume through the defaultMode property.

Because tmpfs is used, the sensitive data stored in the Secret is never written
to disk.

```
$ kubectl exec fortune-https -c web-server -- mount | grep certs
tmpfs on /etc/nginx/certs type tmpfs (ro,relatime) 
```

*Exposing a Secret's entries through environment variables*

```
env:
  - name: FOO_SECRET
    valueFrom:
      secretKeyRef:
        name: fortune-https
        key: foo
```

Similar to regular env vars. Secrets in env var may be exposed inadvertently.
Libraries may log or print them. Child processes also have access to all env
vars.
