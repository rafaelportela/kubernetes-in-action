# Chap 5 - Services: enabling clients to dicover and talk to pods

* Pods are ephemeral - they come and go at any time.
* Kubernetes assigns IPs after the pods is scheduled and before it
  starts - clients can't know its IP before hand.
* Horizontal scaling means multiple pods may provide the same
  service - clients shouldn't have to keep a list of all IPs. Instead, all
those pods should be accessible through a single IP.


## 5.1 Introducing services

Service is a resource to make a single, constant point of entry to a group of
pods providing the same service. Each service has an IP address and port that
never change while the service exists.

### 5.1.1 Creating services

With a given ReplicationController, you can use `kubectl expose` to create
a Service resource with the same pod selectors to expose all pods through
a single IP address and port.

```
apiVersion: v1
kind: Service
metada:
  name: timber-backend
spec:
  ports:
  - port: 80              # port the service will be available on
    targetPort: 8080      # the container port the service will forward to
  selector:
    app: timber-backend   # all pods matching it will be part of the service
```

```
$ kubectl get svc

NAME                TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S) AGE
kubernetes          ClusterIP      10.96.0.1       <none>        443/TCP 3d20h
timber-backend      ClusterIP      10.109.42.181   <none>        8080/TCP 21s
```

The IP address assigned to the service is `10.109.42.181`. This is a cluster
IP, so it's only accessible from inside the cluster. The primary purpose of
services is exposing groups of pods to other pods in the cluster.

To send requests to your service from within the cluster:
* create a pod that will send the request, and possibly log the response.
* ssh into one of the Kubernetes nodes and curl the service.
* curl the service from an existing pod using `kubectl exec` command.

```
$ kubectl exec timber-backend-zz87s -- curl -s http://10.109.42.181:8080/api/v1/colors
{
  "hostname": "2cfb92",
  "state": "3171b5",
  "version": "4882a8"
}
```

Services can specify a `sessionAffininity`, ClientIP or (default) None. There's
no cookie-based session affinity because services work on TCP and UDP levels,
not HTTP.

*Using named ports*

```
apiVersion: v1
kind: Service
spec:
  ports:
  - name: http
    port: 80
    targetPort: http
  - name: https
    port: 443
    targetPort: https
```

If ports are named, you can change port numbers in the pod spec while keeping
the port's name unchanged. As you spin up pods with the new ports, client
connections will be forwarded to the appropriate port numbers, depending on the
pod receiving the connection (port 8080 on old pods and port 80 on the new
ones).

### 5.1.2 Discovering services

Services (hosts and ports) are exposed to pods via env vars. If a service is
running in the cluster, an application (a pod) could inspect its own env vars
to discover services.

```
$ kubectl exec timber-backend-mtnwv env

PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=timber-backend-mtnwv
TIMBER_BACKEND_SERVICE_HOST=10.109.42.181
TIMBER_BACKEND_SERVICE_PORT=8080
...
```

*Discovering services through DNS*

```
$ kubectl run myubuntu -it --image ubuntu -- bash

# cat /etc/resolv.conf
nameserver 10.96.0.10
search default.svc.cluster.local svc.cluster.local cluster.local
options ndots:5

# curl http://timber-backend:8080
# curl http://timber-backend.default:8080
# curl http://timber-backend.svc.cluster.local:8080
```

Ping doesn't work in this case becuase the service's IP is a virtual IP, and
only has meaning when combined with the service port.


## 5.2 Connecting to services living outside the cluster


### 5.2.1 Introducing service endpoints

Services don't link to pods directly. Instead, a resource sits in between - the
Endpoints resource.

```
$ kubectl get endpoints timber-backend
NAME             ENDPOINTS                                         AGE
timber-backend   172.17.0.7:8080,172.17.0.8:8080,172.17.0.9:8080   119m
```

### 5.2.2 Manually configuring service endpoints

Service that will accept incoming connections on port 80. No pod selector is
defined.
```
apiVersion: v1
kind: Service
metadata:
  name: external-service
spec:
  ports:
  - port: 80
```

Because the service has no selector, the corresponding Endpoints resource
hasn't been created automatically.

```
apiVersion: v1
kind: Endpoint
metadata:
  name: external-service   # name should match service's name
subsets:
  - addresses:
    - ip: 11.11.11.11      # service will forward connections
    - ip: 22.22.22.22      # to these IPs
    ports:
    - port: 80
```

New containers will have the env vars for the service with IP and port pairs
and connections will be load balanced to service's endpoint. If you later
decide to migrate the external service to pods running inside Kubernetes, you
can add a selector to the service, thereby making its Endpoints managed
automatically.

### 5.2.3 Creating an alias for an external service

*Creating an ExternalName service*

```
apiVersion: v1
kind: Service
metadata:
  name: external-service
spec:
  type: ExternalName                          # ExternalName service type
  externalName: someapi.somecompany.com       # fqdn
  ports:
  - port: 80
```

After the service is created, pds can connect to the external service through
the `external-service.default.svc.cluster.local` domain name, or even
`external-service`, instead of using the service's actual FQDN. This hides the
actual service anme and its location from pods consuming the service.

At some point later, you may change the externalName attr or change the type
back to ClusterIP and create Endpoint resources for the service. As described,
you can created it manually or automatically by defining label selectors.

ExternalName services are implemented solely at the DNS level, a simple CNAME
DNS record is created for the service. That means, the external service is
accessed directly, bypassing the service proxy completely.


## 5.3 Exposing services to external clients

To expose a service outside the cluster:
* Set the service type to *NodePort* - each cluster node opens a port on the
  node itself and redirects traffic to the service.
* Set the service type to *LoadBalancer*, and extension of the NodePort - makes
  the service accessible through a dedicated load balancer, provisioned from
the cloud infrastructure where Kubernetes is running on. The LB redirects
traffic to node ports across all the nodes.
* Creating an *Ingress* resource, a different mechanism to expose multiple
  services through a single IP address.

### 5.3.1 Using a NodePort service

By creating a NodePort service, you make Kubernetes reserve the same port
number on all its nodes to forward incoming connections to pods that are part
of the service.

It's similar to ClusterIP (service accessible thru/to internal cluster IP). But
NodePort can be accessible by the node's IP too.

```
apiVersion: v1
kind: Service
metadata:
  name: timber-backend
spec:
  type: NodePort
  ports:
  - port: 80               # port at internal cluster IP
    targetPort: 8080       # port in the pods
    nodePort: 30123        # port at the cluster nodes, external IPs
  selector:
    app: timber-backend
```

Hit the service from your laptop (external IP, not minikube cluster):
```
curl http://$(minikube ip):31000
```

The pods are all accessible to the network (quiçá internet?) on any of your
nodes. It's still a good idea to put the node behind a LB so clients don't send
requests to a single node.

### 5.3.2 Exposing a service through an external load balancer

Just setting the `service.spec.type` as LoadBalancer will make the cloud
provider (e.g. GKE) provision a LB for the service.

### 5.3.3 Peculiarities of external connections

Incoming connections may be forwarded to pods from a different node than the
one receiving the connection.
```
spec:
  externalTrafficPolicy: Local
```
The external traffic policy flag will ensure no extra network hop is performed
and all traffic is forward to the same node. You need to make sure all nodes
have pods from the service. Be aware traffic may no be perfectly balanced
across all available nodes.


## 5.4 Exposing services externally through an Ingress resource

Ingresses operate at the HTTP layer. An Ingress controller should be running in
the cluster to provide ingress functionality and different Kubernetes
environments use different implementation of the controller.

GKE uses Google Cloud LBs features to provide the Ingress functionality.
Minikube now includes an add-on that can be enabled to let you try out the
Ingress functionality.

```
minikube addons list
minikube addons enable ingress
```

### 5.4.1 Creating an Ingress resource

```
apiVersion: extensions/v1beta1
kind: Ingress
metada:
  name: timber
spec:
  rules:
  - host: timber.example.com            # maps a domain to a your service
    http:
      paths:
      - path: /                         # requests will to sent to /
        backend:                        #
          serviceName: timber-backend   # for this service
          servicePort: 80               # at this port
  tls:
  - hosts:
    - timber.example.com
    secretName: tls-secret              # should be created (below)

```

Note: In GKE, Ingress controllers require a NodePort type of service.

### 5.4.2 Accessing the service through the Ingress

In `/etc/hosts` add the line:
```
192.168.99.100  timber.example.com
```

Then `curl http://timber.example.com:31000` should hit the running service.

The HTTP request has the header `Host: timber.example.com`, which is used by
the ingress to find the appropriate service. The Ingress Controller didn't
forward the request to the service, it only used it to select a pod. Most, if
not all, controllers works like this.

### 5.4.4 Configuring Ingress to handle TLS traffic

```
openssl genrsa -out tls.key 2048                       # out: tls.key
openssl req -new -x509 -key tls.key -out tls.cert \    # out: tls.cert
    -days 360 -subj /CN=timber.example.com

kubectl create secret tls tls-secret --cert=tls.cert --key=tls.key
```


## 5.5 Introducing readness probes

As soon as a new pod with proper labels is created, it becomes part of hte
service and requests start to be redirected to the pod. But what if the pod
isn't ready to start serving requests immediately?

### 5.5.1 Introducing readiness probes

Similar to liveness probe, readness probes:
* Exec probe
* HTTP GET probe
* TCP socket probe

Service -> Endpoints -> pods

If a readness probe fails, the pod is removed from the Endpoints object. The
effect is the same as when the pod doesn't match the service's label selector
at all.

If a group of pods depends on a service provided by another pod, a database for
example, if may be wise for its readness probe to signal to Kubernetes that the
pod ins't read to serve any requests at that time.

### 5.5.2 Adding a readiness probe to a pod

```
apiVersion: v1
kind: ReplicationController
...
spec:
  ...
  template:
    ...
    spec:
      containers:
      - name: timber-backend
        image: registry/timber-backend
        ...
        readinessProbe:
          httpGet:
            path: /probe/readiness
            port: 8080
            scheme: HTTP
          initialDelaySeconds: 10
          timeoutSeconds: 2
```

### 5.5.3 Understanding what real-world readness probes should do

Applications can take a few seconds to start up. You should always define
a readness probe, even if it's as simple as sending an HTTP request to the base
url, of clients may see "connection refused" erros during the startup period.


## 5.6 Using a headless service for discovering individual pods

Services can provide a stable IP address to clients to connect to pods (or
other endpoints) backing each service. Each connection to the service forwarded
to one randomly selected backing pod. But what if the client needs to connect
to *more than one* or *all* the other pods?

Usually, in Kubernetes, a DNS lookup will return a single IP - the service's
cluster IP. But if you set `ClusterIP: None` in the service, the DNS
will return the pod IPs instead of the single service IP. Clients can do
a regular DNS A record lookup and get the IPs of all the pods that are part of
the service.
