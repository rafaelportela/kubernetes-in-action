# Chap 6 Volumes: attaching disk storage to containers


## 6.1 Introducing volumes

Kubernetes volumes are a component of a pod and are thus defined in the pod's
specification - much like containers. They aren't a standalone Kubernetes
object and cannot be created or deleted on their own.

It's not enough to define a volume in the pod; you need to define
a *VolumeMount* inside the containers's spec also, if you want the container to
be able to access it.

Some of the available volume types:
* gitRepo
* nfs
* gcePersistentDisk, awsElasticBlockStore, azureDisk
* cinder, cephfs, iscsi
* configMap, secret, downwardAPI
* persistentVolumeClaim


## 6.2 Using volumes to share data between containers


```
apiVersion: v1
kind: Pod
metadata:
  name: fortune
spec:
  containers:
    - image: luksa/fortune
      name: html-generator
      volumeMounts:
        - name: html                          # attach 'html' volume
          mountPath: /var/htdocs
    - image: nginx:alpine
      name: web-server
      volumeMounts:
        - name: html                          # attach 'html' volume
          mountPath: /usr/share/nginx/html
          readOnly: true
      ports:
        - containerPort: 80
          protocol: TCP
  volumes:
    - name: html                              # declare 'html' volume
      emptyDir: {}
```

```
kubectl port-forward fortune 8080:80
curl http://localhost:8080
```

`emptyDir` and `gitRepo` volumes are not persisted, they get deleted when a pod
is torn down.


## 6.3 Accessing files on the worker node's filesystem

Certain system-level pods (these will usually be managed by a DaemonSet) do
need to either read the node's files or use the node's filesystem to cess the
node's devices through the filesystem. Kubernetes makes this possible through
a `hostPath` volume.

It's not a good idea to use hostPath volumes for database store, or even for
regular pods, because if the pod gets rescheduled to another node it will no
longer see the data.


## 6.4 Using persistent storage

When data needs to be accessible from any cluster node, it must be stored on
some type of network-attached storage (NAS).

### 6.4.1 Using GCE Persistent Disk in a pod volume

Create a GCE persistent virtual disk called mongodb.
```
gcloud comput disks create --size=1GB --zone=europe-west1-b mongodb
```

Create a pod running MongoDB attaching the created disk.
```
apiVersion: v1
kind: Pod
metadata:
  name: mongodb
spec:
  volumes:
    - name: mongodb-data
      gcePersistentDisk:
        pdName: mongodb      # must match the name of a created GCE disk
        fsType: ext4
      #hostPath:             # use hostPath if minikube
      #  path: /tmp/mongodb  
  containers:
    - image: mongo
      name:  mongodb
      volumeMounts:
        - name: mongodb-data
          mountPath: /data/db
      ports:
        - containerPort: 27017
          protocol: TCP
```

```
kubectl exec -it mongodb mongo
> use mystore
> db.foo.insert({name:'foo'})
> db.foo.find()
```

Now the pod could be delete and recreated, the data should still be available.


## 6.5 Decoupling pods from the underlying storage technology

The volume explored so far require the developer to know the actual network
storage infrastructure (nfs, gce disks, hostPath) available in the cluster.
Ideally, a dev deploying their apps on Kubernetes should never have to know
what kind of storage technology is used underneath, the same way they don't
have to know what type of physical servers are being used to run their pods.

### 6.5.1 Introducing PersistentVolumes and PersistentVolumeClaims

PersistentVolumes are provisioned by cluster admins and consumed by pods through
PersistentVolumeClaims.

[admin] -> registers a "PersistentVolume" resource, specifies size and access mode

[user/dev] -> applies PersistenVolumeClaim mainfest -> k8s finds the appropriate PersistentVolume

### 6.5.2 Creating a PersistentVolume

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: mongodb-pv
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce                     # supports single client for writing
    - ReadOnlyMany                      # supports multple clients for reading
  persistentVolumeReclaimPolicy: Retain # after claim is released, the PV should *not* be deleted
  #hostPath:
  #  path: /tmp/mongodb                  # use hostPath for minikube
  gcePersistentDisk:
    pdName: mongodb
    fsType: ext4
```

PersistentVolumes don't belong to any namespace. They're cluster-level
resources like nodes, unlike pods and PersistentVolumeClaims.

### 6.5.3 Claiming a PersistentVolume by creating a PersistentVolumeClaim

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mongodb-pvs
spec:
  resources:
    requests:
      storage: 1Gi
  accessModes:
    - ReadWriteOnce      # single client for reads and writes
  storageClassName: ""   # dynamic provisioning
```

```
$ kubectl apply -f mongodb-pvc.yaml
$ kubectl get pvc
NAME          STATUS   VOLUME       CAPACITY   ACCESS MODES   STORAGECLASS AGE
mongodb-pvs   Bound    mongodb-pv   1Gi        RWO,ROX                       5s
```

Access modes:
* RWO ReadWriteOnce - single node can mount the volume for reading and writing
* ROX ReadOnlyMany - multiple nodes can read
* RWX ReadWriteMany - multiple nodes can both read and write

RWO, ROX and RWX pertain to the number of worker nodes that can use the volume
at the same time, not to the number of pods.

### 6.5.4 Using a PersistentVolumeClaim in a pod

```
apiVersion: v1
kind: Pod
metadata:
  name: mongodb
spec:
  containers:
    - image: mongo
      name: mongodb
      volumeMounts:
        - name: mongodb-data
          mountPath: /data/db
      ports:
        - containerPort: 27017
          protocol: TCP
  volumes:
    - name: mongodb-data
      persistentVolumeClaim:
        claimName: mongodb-pvc  # ref. the PVC by name in the pod
```

### 6.5.6 Reclycling PersistentVolumes

If a pod and its PersistentVolumeClaim is deleted, I would expect that the
underlying PersistentVolume would become free again, ready to be reused. But
that's not what happens.

```
kubectl delete pod mongodb
kubectl delete pvc mongodb-pvc
```

If the mongodb-pvc is recreated, it will be in pending state because the
existing PersistentVolume will actually be in "released" state, instead of in
"available" state.

Pods using a PersistentVolume claim can come and go, the pvc object can be kept
in the cluster. If a claim is deleted, another one cannot be bound to the
persistent volume. The PV should be delete and recreated (manually) or it
should have a reclaim policy as `Recycle`, volumes content will be delete and
the volume will become available again, or as `Delete`, where the storage is
deleted.

```
$ kubectl describe pv mongodb-pv
Name:            mongodb-pv
...
Labels:          <none>
Status:          Bound
Claim:           default/mongodb-pvc
Reclaim Policy:  Retain                  # no auto clean up
Access Modes:    RWO,ROX
VolumeMode:      Filesystem
```

The PV manifest can be updated with:
```
spec:
  persistentVolumeReclaimPolicy: Recycle

  # or
  #persistentVolumeReclaimPolicy: Delete    # storage volume (AWS EBS, GCE disk, Cinder vol) is deleted
```

Note: Reclaim policy is deprecated. Dynamic provisioning should be used
instead.


## 6.6 Dynamic provisioning of PersistentVolumes

Kubernetes releases the developer tof having to deal with the actual storage
technology used underneath, but still requires a cluster admin to provision the
actual storage up front.

Admins can deploy a PersistentVolume provisioner and define one or more
StorageClass object to let users choose what type of PersistentVolume they
want. The users can refer to the StorageClass in their PErsistentVolumeClaims
and the provisioner will take that into account when provisioning the
persistent storage.

Similar to PersistentVolumes, StorageClass resources aren't namespaced. 

Provisioners for the most populart cloud providers are available. If it's on
premises, a custom provider needs to be deployed.

### 6.6.1 Defining the available storage types through StorageClass resources

```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: fast
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-ssd
  zone: europe=west1-b
```

### 6.6.2 Requesting the storage class in a PersistentVolumeClaim

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mongodb-pvc
spec:
  storageClassName: fast
  resources:
    requests:
      storage: 100<o
  accessModes:
    - ReadWriteOnce
```

```
kubectl get pvc mongodb-pvc
kubectl get pv
kubectl get sc
gcloud compute disks list
```

### 6.6.3 Dynamic provisioning without specifying a storage class

If storage class is omitted, the default storage class will be used.

```
$ kubectl get sc standard
NAME                 PROVISIONER                AGE
standard (default)   k8s.io/minikube-hostpath   21h
```

Minikube will use a hostpath volume, GCE will provision a Persistent Disk.

*Creating a PersistentVolumeClaim without specifying a storage class*

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mongodb-pvc2
spec:
  resources:
    requests:
      storage: 100Mi
  accessModes:
    - ReadWriteOnce
  # storageClassName: "" # if empty string, a pre-provisioned PersistentVolume should exist
                         # if omitted, the default StorageClass will be provisioned
```

*Understanding the complete picture of dynamic PersistentVolume provisioning*

```
[admin] -> sets up a PersistentVolume provisioner
        -> creates a StorageClass for it, opt marks it as default SC

[user]  -> creates a PVC referencing the StorageClass, or none if default

[k8s]   -> looks up the SC reference and requests provisioner a PV

[user]  -> creates pods with volume referencing  PVC by name
```
