# Chap 4 - Replication and other controllers: deploying managed pods


## 4.1 Keeping pods healthy

*Liveness probes*

* Http GET probe
* TCP socket probe
* Exec probe

```
spec:
  containers:
  - name: timber-backend
    image: registry/timber-backend
    livenessProbe:
      httpGet:
        path: /
        port: 8080
```

The Kubelet on the node will restart pods if they crash or if they liveness
probes fail. But if the node itself crashes, it's the Control Plane that must
create replacements for all the pods that went down with the node.

To make sure your app is restarted on another node, you need to have the pod
managed bu a Replication Controller or similar mechanism.


## 4.2 Introducing ReplicationControllers

If a pod disappers for any reason the ReplicationController notices the missing
pod and creates a replacement pod.

* label selector
* replica count
* pod template

```
apiVersion: v1
kind: ReplicationContrller
metadata:
  name: timber-backend
spec:
  replicas: 3
  selector:                 # pod selector determining what oids
    app: timber-backend     # the RC is operating on
  template:
    metadata:
      labels:
        app: timber-backend
    spec:
      containers:
      - name: timber-backend
        image: registry/timber-backend
```

TIP: if pod selector is omitted, the Replication Controller will use the one
difined in the pod template.

### 4.2.4 Moving pods in and out of the scope of a RC

The API server allows clients to watch for changes to resources and resource
lists. Replication Controllers are immediatelynotified about a pod being
deleted. The notification triggers the controller to check the actial number of
pods and take appropriate action.

```
kubectl get pods --show-labels                               # it lists 3 pods
kubectl label pod timber-backend-2bcrt type=special
kubectl label pod timber-backend-2bcrt app=foo --overwrite   # the RC will no longer
                                                             # manage this pod
```

If you know a pod is malfunctioning, you can take it out of the Replication
Controller's scope, let the controller replace it with a new one, and then
debug or play with the pod. Once you've done, you delete the pod.

### 4.2.5 Changing the pod template

Pod templates can be modified at any time, but it will only affect the pods you
replace afterwards - no pods will be replaced immediatelly, only if it gets
deleted.

Edit labels of pod's template (include a new one). Existing pods are not affected.
```
kubectl edit rc timber-backend
kubectl get pods --show-labels
kubectl delete pod timber-backend-d2lw6
kubectl get pods --show-labels            # one new pod with the new label
```

### 4.2.6 Horizontally scaling pods

```
kubectl scale rc timber-backend --replicas=10

# or
kubectl edit rc timber-backend 

kubectl get rc
```

### 4.2.7 Deleting a Replication Controller

Deleting a RC also delete its managed pods. Considering Pods are not integral
part of a Replication Controller - they are only managed by one - RC can be
delete while keeping pods running.

```
kubect delete rc timber-backend --cascade=false
```

The pods will no longer be managed. You can always create a new Replication
Controller with the proper label selectors and make them managed again.


## 4.3 Using ReplicaSets instead of ReplicationControllers

Usually, ReplicaSets are not created directly, but instead created
automatically when you create the higher-level Deployment resource.

ReplicaSets will eventually depracate ReplicationControllers. While RC select
pods matching a certain label, ReplicaSets have more expressive selectors. Pods
can also be selected by lacking a label or including a label regardless of its
value

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: timber-backend
spec:
  replicas: 3
  selector:
    matchLabels:                # selectors are inside selecto.matchLabels
      app: timber-backend
  template:
    metadata:
      labels:
        app: timber-backend
    spec:
      containers:
      - name: timber-backend
        image: registry.gitlab.com/containersolutions/timber/backend
        ports:
        - containerPort: 8080
```

If the previous Replication Controller was deleted with `--cascade=false`, this
new ReplicaSet will start to manage the same pods, becase the defined
selectors.

More expressive label selectors:
* In - label value must match specified values.
* NotIn - label must not match value
* Exists - pods must include a label, the value ins't important
* DoesNotExist - pod doesn't include the label

```
selector:
  matchExpressions:
    - key: app
      operator: In
      values: timber-backend
```


## 4.4 Running exactly one pod on each node with DaemonSets

Those cases include infrastructure-related pods that perform system-level
operations. For example, you'll want to run a log collector and a resource
monitor on every node. Another good example is Kubernetes' own *kube-proxy*
process, which needs to run on all node to make services work.

Outside of Kubernetes, such processes would usually be started through initd
scripts or systemd during boot up. You can still do it, but you won't benefit
from Kubernetes features.

Example: a DaemonSet to run a ssd-monitor process on each node with `disk=ssd` 
label.


## 4.5 Running pods that perform a single completable task

ReplicationControllers, ReplicaSets and DaemonSets run continuous taasks that
are never considered completed. Processes in such pods are restarted when they
exit. But in a completable task, after its processes terminates, it should not
be restarted again.

Opposed to unmanaged pods, Pods managed by a *Batch Reource* might still be
reschedule in case it's terminated (node is down, pod is evicted). In case it
completes, it won't be rescheduled anymore.


## 4.6 Scheduling Jobs to run periodically or once in the future

Similar to unix's cronjob, a Batch Resource can have a schedule string.
